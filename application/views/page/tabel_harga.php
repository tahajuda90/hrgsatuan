<?php if ($last_survei!=null){
     echo '<div class="row">
        <div class="callout callout-info">
            <h5>Update Terakhir</h5>
            <p>'. fdateindo($last_survei->tgl_survei).' </p>
        </div>
    </div>';
}?>

<div class="row">
    <div class="col-3">
        <form action="<?= base_url('tampilfront/index')?>" method="post">
        <div class="card card-warning">
            <div class="card-header">
                <h3 class="card-title">Filter</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                        <label>Pilih Kategori</label>
                        <select class="form-control select2" name="id_kategori" id="id_kategori" style="width: 100%;">
                            <?php
                            foreach($kategori as $ktgr){
                                ?>
                            <option value="<?=$ktgr->id_kategori?>"><?=$ktgr->kategori?></option>
                                    
                                    <?php
                            }
                            ?>
                        </select>
                </div>
                <div class="form-group">
                    <label >Tanggal Survei</label>
                    <input type="text" class="form-control" name="tgl_survei" id="tgl_survei" value="" />
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Filter</button> 
            </div>
        </div>
        </form>
    </div>
    <div class="col-9">
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title">Daftar Harga Barang Berdasarkan Tiga Kecamatan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered">
                    <thead>
                        <tr>
                            <th rowspan="2" style="text-align:center">No</th>
                            <th rowspan="2">Nama</th>
                            <th rowspan="2">Satuan</th>
                            <th colspan="3" style="text-align:center">Harga</th>
                        </tr>
                        <tr>
                            <th>Pesantren</th>
                            <th>Kota</th>
                            <th>Mojoroto</th
                        </tr>
                        
                    </thead>
                    <tbody>
                        <?php
                        if(!empty($barang)){
                            $no=0;
                            foreach($barang as $brg){ 
                                $no++;
                                ?>
                        <tr>
                            <td>
                                <?=$no?>
                            </td>
                            <td>
                                <?=ucfirst($brg->nama)?>
                            </td>
                            <td>
                                <?= ucfirst($brg->satuan)?>
                            </td>
                            <td>
                                <?= pembulatan($brg->HargaPesantren)?>
                                <br>
                                <button type="button" class="btn btn-primary btn-xs" id="<?=$brg->PnydPesantren?>" data-toggle="modal" data-target="#modal-lg">Penyedia <i class="fas fa-info"></i></button>
                            </td>
                            <td>
                                <?= pembulatan($brg->HargaKota)?>
                                <br>
                                <button type="button" class="btn btn-primary btn-xs" id="<?=$brg->PnydKota?>" data-toggle="modal" data-target="#modal-lg">Penyedia <i class="fas fa-info"></i></button>
                            </td>
                            <td>
                                <?= pembulatan($brg->HargaMojoroto)?>
                                <br>
                                <button type="button" class="btn btn-primary btn-xs" id="<?=$brg->PnydMojoroto?>" data-toggle="modal" data-target="#modal-lg">Penyedia <i class="fas fa-info"></i></button>
                            </td>
                        </tr>
                        
                        <?php                                
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
</div>
<div class="modal fade" id="modal-lg">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Informasi Penyedia</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body edit-content">
                <h1>
                  Nama Penyedia
                </h1> 

                <hr>
                
                <strong> Npwp</strong>
                
                <p class="text-muted">00.000.000.0-000.000</p>
                
                <span class="tag tag-info">PTKP</span>               
                
                <hr>
                <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

                <p class="text-muted">Malibu, California</p>
                <p class="text-muted">072412746108</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
    $("#example1").DataTable();
    $('#example2').DataTable({
      paging: true,
      scrollY:"300px",
      scrollX:true,
      scrollCollapse: true,
      searching: true,
      info: true,
      autoWidth: false,
      fixedColumns:   {
            leftColumns: 1
        }
    });
    $('#modal-lg').on('show.bs.modal',function(e){
        var $modal = $(this),
        pnyid = e.relatedTarget.id;
        
        $.ajax({
            cache: false,
            url: '<?= base_url('tampilfront/info_penyedia/')?>'+pnyid,
            dataType : 'html',
            success: function(data) {
                $modal.find('.edit-content').html(data);
            }
        });
        
        
    });
});
</script>