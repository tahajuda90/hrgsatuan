<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dt_penyedia extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Dt_penyedia_model','kecamatan_model'));
        $this->load->library('form_validation');
        if(!$this->ion_auth->logged_in()){
            redirect('Auth','refresh');
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'dt_penyedia?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'dt_penyedia?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'dt_penyedia';
            $config['first_url'] = base_url() . 'dt_penyedia';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Dt_penyedia_model->total_rows($q);
        $dt_penyedia = $this->Dt_penyedia_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'page'=>'page_penyedia',
            'dt_penyedia_data' => $dt_penyedia,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('main', $data);
    }

    public function read($id) 
    {
        $row = $this->Dt_penyedia_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_penyedia' => $row->id_penyedia,
		'npwp' => $row->npwp,
		'alamat' => $row->alamat,
		'nama_penyedia' => $row->nama_penyedia,
		'status_pjk' => $row->status_pjk,
		'telepon' => $row->telepon,
		'created_date' => $row->created_date,
		'update_date' => $row->update_date,
	    );
            $this->load->view('dt_penyedia/dt_penyedia_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_penyedia'));
        }
    }

    public function create() 
    {
        $data = array(
            'page' => 'page_form_penyedia',
            'button' => 'Create',
            'kecamatan'=>$this->kecamatan_model->get_all(),
            'action' => site_url('dt_penyedia/create_action'),
	    'id_penyedia' => set_value('id_penyedia'),
            'id_kecamatan' => set_value('id_kecamatan'),
	    'npwp' => set_value('npwp'),
	    'alamat' => set_value('alamat'),
	    'nama_penyedia' => set_value('nama_penyedia'),
	    'status_pjk' => set_value('status_pjk'),
	    'telepon' => set_value('telepon')
	);
        $this->load->view('main', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
		'npwp' => $this->input->post('npwp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'nama_penyedia' => $this->input->post('nama_penyedia',TRUE),
		'status_pjk' => $this->input->post('status_pjk',TRUE),
		'telepon' => $this->input->post('telepon',TRUE),
		'created_date' => date("Y-m-d H:i:s")
	    );

            $this->Dt_penyedia_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('dt_penyedia'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Dt_penyedia_model->get_by_id($id);

        if ($row) {
            $data = array(
                'page' => 'page_form_penyedia',
                'button' => 'Update',
                'kecamatan'=>$this->kecamatan_model->get_all(),
                'action' => site_url('dt_penyedia/update_action'),
		'id_penyedia' => set_value('id_penyedia', $row->id_penyedia),
                'id_kecamatan' => set_value('id_kecamatan',$row->id_kecamatan),
		'npwp' => set_value('npwp', $row->npwp),
		'alamat' => set_value('alamat', $row->alamat),
		'nama_penyedia' => set_value('nama_penyedia', $row->nama_penyedia),
		'status_pjk' => set_value('status_pjk', $row->status_pjk),
		'telepon' => set_value('telepon', $row->telepon)
	    );
            $this->load->view('main', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_penyedia'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_penyedia', TRUE));
        } else {
            $data = array(
                'id_kecamatan' => $this->input->post('id_kecamatan',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
		'npwp' => $this->input->post('npwp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'nama_penyedia' => $this->input->post('nama_penyedia',TRUE),
		'status_pjk' => $this->input->post('status_pjk',TRUE),
		'telepon' => $this->input->post('telepon',TRUE),
                'created_date' => date("Y-m-d H:i:s")
	    );

            $this->Dt_penyedia_model->update($this->input->post('id_penyedia', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('dt_penyedia'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Dt_penyedia_model->get_by_id($id);

        if ($row) {
            $this->Dt_penyedia_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('dt_penyedia'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_penyedia'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('npwp', 'npwp', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	$this->form_validation->set_rules('nama_penyedia', 'nama penyedia', 'trim|required');
	//$this->form_validation->set_rules('status_pjk', 'status pjk', 'trim|required');
	$this->form_validation->set_rules('telepon', 'telepon', 'trim|required');
	//$this->form_validation->set_rules('created_date', 'created date', 'trim|required');
	//$this->form_validation->set_rules('update_date', 'update date', 'trim|required');

	$this->form_validation->set_rules('id_penyedia', 'id_penyedia', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "dt_penyedia.xls";
        $judul = "dt_penyedia";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Npwp");
	xlsWriteLabel($tablehead, $kolomhead++, "Alamat");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Penyedia");
	xlsWriteLabel($tablehead, $kolomhead++, "Status Pjk");
	xlsWriteLabel($tablehead, $kolomhead++, "Telepon");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Update Date");

	foreach ($this->Dt_penyedia_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->npwp);
	    xlsWriteLabel($tablebody, $kolombody++, $data->alamat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_penyedia);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status_pjk);
	    xlsWriteLabel($tablebody, $kolombody++, $data->telepon);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->update_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Dt_penyedia.php */
/* Location: ./application/controllers/Dt_penyedia.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-25 07:29:09 */
/* http://harviacode.com */