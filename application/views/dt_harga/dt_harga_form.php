<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Dt_harga <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Id Brg <?php echo form_error('id_brg') ?></label>
            <input type="text" class="form-control" name="id_brg" id="id_brg" placeholder="Id Brg" value="<?php echo $id_brg; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Id Penyedia <?php echo form_error('id_penyedia') ?></label>
            <input type="text" class="form-control" name="id_penyedia" id="id_penyedia" placeholder="Id Penyedia" value="<?php echo $id_penyedia; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Harga <?php echo form_error('harga') ?></label>
            <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga" value="<?php echo $harga; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Created Date <?php echo form_error('created_date') ?></label>
            <input type="text" class="form-control" name="created_date" id="created_date" placeholder="Created Date" value="<?php echo $created_date; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Update Date <?php echo form_error('update_date') ?></label>
            <input type="text" class="form-control" name="update_date" id="update_date" placeholder="Update Date" value="<?php echo $update_date; ?>" />
        </div>
	    <input type="hidden" name="id_objk" value="<?php echo $id_objk; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('dt_harga') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>