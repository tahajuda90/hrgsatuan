 <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Jenis Barang</h3>

                <div class="card-tools">
                     <form action="<?php echo site_url('dt_barang_jasa/index'); ?>" class="form-inline" method="get">
                    <div class="input-group input-group-sm" style="width: 200px;">
                    <select class="form-control select2" name="qktgr" id="id_kategori" style="width: 100%;">
                        <option></option>
                        <?php
                        foreach($kategori as $ktgr){
                            ?>
                        <option <?=($ktgr->id_kategori==$qktgr) ? "selected" : ""?> value="<?=$ktgr->id_kategori?>"><?=$ktgr->kategori?></option>

                                <?php
                        }
                        ?>
                    </select>
                    <input type="text" name="q" value="<?php echo $q; ?>" class="form-control float-right" placeholder="Search">
                    
                    <div class="input-group-append">
                        <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('dt_barang_jasa'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>                  
                </div>
              </div>
              <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th>Nama</th>
                        <th>Satuan</th>
                        <th>Spesifikasi</th>
                        <th>Tanggal Dibuat</th>
                        <th>Action</th>
                    </tr>
                  </thead>
                  <tbody><?php
            foreach ($dt_barang_jasa_data as $dt_barang_jasa)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $dt_barang_jasa->kategori ?></td>
			<td><?php echo $dt_barang_jasa->nama ?></td>
			<td><?php echo $dt_barang_jasa->satuan ?></td>
			<td><?php echo $dt_barang_jasa->spesifikasi ?></td>
			<td><?php echo  fdateindo($dt_barang_jasa->created_date) ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('dt_barang_jasa/update/'.$dt_barang_jasa->id_objek),'Update'); 
				echo ' | '; 
				echo anchor(site_url('dt_barang_jasa/delete/'.$dt_barang_jasa->id_objek),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
                  </tbody>
                </table>
                  
              </div>
              <?php echo $pagination ?>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->