<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Daftar Penyedia Hasil Survei</h3>
                </div>
                <div class="card-body table-responsive p-0">
                    <button onclick="window.location.href='<?= base_url('dt_survei/create_detail/'.$idsurvei)?>'" class="btn btn-primary"><i class="fas fa-plus-square"></i> Penyedia</button>
                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>BA. Hasil Survei</th>
                                <th>Nama Penyedia</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $start=0;
            foreach ($dt_penyedia_data as $dt_penyedia)
            {
                ?>
                            <tr>
                                <td width="80px"><?php echo ++$start ?></td>
                                <td><?php echo $dt_penyedia->nmr_survei ?></td>
                                <td><?php echo $dt_penyedia->nama_penyedia ?></td>
                                <td><a class="btn btn-block btn-success btn-sm" type="button" href="<?= site_url('entri_survei/index/'.$dt_penyedia->id_survei.'?prvdr='.$dt_penyedia->id_penyedia)?>">Item Produk</a></td>
                            </tr>
                            <?php
            }
            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
