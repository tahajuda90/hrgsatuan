<div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Masukan Harga</h3>
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 250px;">                      
                      <form action="<?php echo site_url('entri_survei/index'); ?>" class="form-inline" method="get">
                          <input type="hidden" id="prvdr" name="provider" value="<?= $pnyd ?>" />
                          <input type="hidden" id="srvi" name="survei" value="<?= $srv ?>" />
                          <input type="text" name="q" class="form-control float-right" placeholder="Search" />
                          <div class="input-group-append">
                              <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                          </div>
                      </form>                    
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-2">
                  <button onclick="window.location.href='<?= base_url('dt_harga/create/'.$srv.'?prvdr='.$pnyd)?>'" class="btn btn-primary"><i class="fas fa-plus-square"></i> Barang</button>
                  <table id="tbl-surve" class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Satuan</th>
                        <th>Tanggal Update</th>
                        <th>Harga </th>
                        <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php 
                      $num =0;
                      foreach($record as $rcd){
                          $num++
                      ?>
                    <tr>
                      <td><?=$num?><input type="hidden" name="objk" class="form-control" value="<?=$rcd->id_objk?>"/></td>
                      <td><?=$rcd->nama?><input type="hidden" name="objk" class="form-control" value="<?=$rcd->id_brg?>"/></td>
                      <td><?=$rcd->satuan?></td>
                      <td><?= fdateindo($rcd->update_date)?></td>
                      <td><input type="text" class="form-control" name="hrg" value="<?=$rcd->harga?>" /></td>
                      <td><button type="button" class="btn btn-block btn-success btn-xs">edit</button></td>
                    </tr>
                    <?php
                      }
                      ?>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
<script>
$(function(){
    $('#tbl-surve').on('click','button',function(){
        var idobjk =$(this).closest('tr').find("input[name='objk']").val();
        //var idpnyd =$('#prvdr').val();
        //var idsrv =$('#srvi').val();
        var hrg = $(this).closest('tr').find("input[name='hrg']").val();
//        $.post("http://taha.localhost/hrgsatuan/dt_harga/create_action/"+idpnyd,{ id_brg:idbrg,id_penyedia:idpnyd,id_survei:idsrv,harga:hrg}).done(function(){
//            location.reload();
//        });
        $.post("<?= base_url()?>dt_harga/update_action/",{ id_objk:idobjk,harga:hrg}).done(function(){
            location.reload();
        });
    });
});
</script>