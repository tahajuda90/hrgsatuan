<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Daftar Survei</h3>
                </div>
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>BA. Hasil Survei</th>
                                <th>Tanggal Survei</th>
                                <th>Audituser</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
            foreach ($dt_survei_data as $dt_srv)
            {
                ?>
                            <tr>
                                <td width="80px"><?php echo ++$start ?></td>
                                <td><?php echo $dt_srv->nmr_survei ?></td>
                                <td><?php echo fdateindo($dt_srv->tgl_survei) ?></td>
                                <td><?php echo $dt_srv->audituser?></td>
                                <td><a class="btn btn-block btn-primary btn-xs" type="button" href="<?= site_url('dt_survei/detail/'.$dt_srv->id_survei)?>">Detail</a></td>
                            </tr>    
                            <?php
            }
            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
