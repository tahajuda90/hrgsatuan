<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entri_survei_model extends CI_Model {
    //put your code here
    
    private $prvdr = null;
    private $surv = null;
    public function __construct() {
        parent::__construct();
    }
    
    
    public function set_prvdr($provdr){
        $this->prvdr = $provdr;
    }
    
    public function set_survei($surv){
        $this->surv = $surv;
    }
    
    
    public function survei_record($q=NULL){
        $this->db->select(array('dt_harga.*','dt_barang_jasa.nama','dt_barang_jasa.satuan'))->from('dt_harga');
        $this->db->where('dt_harga.id_survei', $this->surv);
        $this->db->where('dt_harga.id_penyedia', $this->prvdr);
        $this->db->like('dt_barang_jasa.nama', $q);
        $this->db->join('dt_barang_jasa','dt_harga.id_brg = dt_barang_jasa.id_objek');
        return $this->db->get()->result();
    }
    
    public function last_record($limit,$start=0,$q=NULL){
        
        $this->db->select(array('dt_harga.id_brg','dt_harga.harga','dt_harga.created_date last','dt_harga.id_penyedia'))
                ->from('dt_harga')
                ->where('dt_harga.id_penyedia', $this->prvdr)
                ->where_in('dt_harga.created_date',"SELECT MAX(dt_harga.created_date) AS created_date FROM dt_harga WHERE dt_harga.id_penyedia = $this->prvdr GROUP BY dt_harga.id_brg",FALSE);
        $subquery = $this->db->get_compiled_select();
        
        $this->db->select(array("dt_barang_jasa.*","t2.harga","t2.last"))->from("dt_barang_jasa");
        $this->db->or_like('dt_barang_jasa.nama', $q);
	$this->db->or_like('dt_barang_jasa.spesifikasi', $q);
        $this->db->limit($limit,$start);
        $this->db->join("($subquery) as t2","dt_barang_jasa.id_objek = t2.id_brg","LEFT");
        $this->db->order_by('dt_barang_jasa.id_objek','ASC');
        
        return $this->db->get()->result();
    }
    
    function total_rows($q = NULL) {
        $this->db->like('id_objek', $q);
	$this->db->or_like('dt_barang_jasa.id_kategori', $q);
	$this->db->or_like('nama', $q);
	$this->db->or_like('satuan', $q);
	$this->db->or_like('spesifikasi', $q);
	$this->db->from('dt_barang_jasa');
        return $this->db->count_all_results();
    }
    

}
