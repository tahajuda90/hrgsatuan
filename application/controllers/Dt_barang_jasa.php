<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dt_barang_jasa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Dt_barang_jasa_model');
        $this->load->library('form_validation');
        if (!$this->ion_auth->logged_in()) {
            redirect('/auth', 'refresh');
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $qktgr = urldecode($this->input->get('qktgr', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'dt_barang_jasa/index?qktgr=' . urlencode($qktgr).'&q='. urlencode($q);
            $config['first_url'] = base_url() . 'dt_barang_jasa/index?qktgr=' . urlencode($qktgr).'&q='. urldecode($q);
        } else {
            $config['base_url'] = base_url() . 'dt_barang_jasa/index';
            $config['first_url'] = base_url() . 'dt_barang_jasa/index';
        }
        

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Dt_barang_jasa_model->total_rows($q);
        $dt_barang_jasa = $this->Dt_barang_jasa_model->get_limit_data($config['per_page'], $start,$qktgr,$q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'page'=>'page_barang_jasa',
            'dt_barang_jasa_data' => $dt_barang_jasa,
            'kategori' => $this->Dt_barang_jasa_model->get_kategori(),
            'qktgr'=>$qktgr,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('main', $data);
        //print_r($dt_barang_jasa);
    }

    public function read($id) 
    {
        $row = $this->Dt_barang_jasa_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_objek' => $row->id_objek,
		'id_kategori' => $row->id_kategori,
		'nama' => $row->nama,
		'satuan' => $row->satuan,
		'spesifikasi' => $row->spesifikasi,
		'created_date' => $row->created_date,
		'update_date' => $row->update_date,
	    );
            $this->load->view('dt_barang_jasa/dt_barang_jasa_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_barang_jasa'));
        }
    }

    public function create() 
    {
        $data = array(
            'page'=>'page_form_barang_jasa',
            'kategori' => $this->Dt_barang_jasa_model->get_kategori(),
            'button' => 'Create',
            'action' => site_url('dt_barang_jasa/create_action'),
	    'id_objek' => set_value('id_objek'),
	    'id_kategori' => set_value('id_kategori'),
	    'nama' => set_value('nama'),
	    'satuan' => set_value('satuan'),
	    'spesifikasi' => set_value('spesifikasi'),
	);
        $this->load->view('main', $data);
        //print_r($data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_kategori' => $this->input->post('id_kategori',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
		'nama' => $this->input->post('nama',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'spesifikasi' => $this->input->post('spesifikasi',TRUE),
		'created_date' => date("Y-m-d H:i:s"),
	    );

            $this->Dt_barang_jasa_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('dt_barang_jasa'));
            //print_r($data);
        }
    }
    
    public function update($id) 
    {
        $row = $this->Dt_barang_jasa_model->get_by_id($id);

        if ($row) {
            $data = array(
                'page'=>'page_form_barang_jasa',
                'kategori' => $this->Dt_barang_jasa_model->get_kategori(),
                'button' => 'Update',
                'action' => site_url('dt_barang_jasa/update_action'),
		'id_objek' => set_value('id_objek', $row->id_objek),
		'id_kategori' => set_value('id_kategori', $row->id_kategori),
		'nama' => set_value('nama', $row->nama),
		'satuan' => set_value('satuan', $row->satuan),
		'spesifikasi' => set_value('spesifikasi', $row->spesifikasi),
	    );
            $this->load->view('main', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_barang_jasa'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_objek', TRUE));
        } else {
            $data = array(
		'id_kategori' => $this->input->post('id_kategori',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
		'nama' => $this->input->post('nama',TRUE),
		'satuan' => $this->input->post('satuan',TRUE),
		'spesifikasi' => $this->input->post('spesifikasi',TRUE),
	    );

            $this->Dt_barang_jasa_model->update($this->input->post('id_objek', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('dt_barang_jasa'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Dt_barang_jasa_model->get_by_id($id);

        if ($row) {
            $this->Dt_barang_jasa_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('dt_barang_jasa'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_barang_jasa'));
        }
    }

    public function _rules() 
    {
	//$this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required');
	$this->form_validation->set_rules('nama', 'nama', 'trim|required');
	$this->form_validation->set_rules('satuan', 'satuan', 'trim|required');
	$this->form_validation->set_rules('spesifikasi', 'spesifikasi', 'trim|required');
	//$this->form_validation->set_rules('created_date', 'created date', 'trim|required');
	//$this->form_validation->set_rules('update_date', 'update date', 'trim|required');

	$this->form_validation->set_rules('id_objek', 'id_objek', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "dt_barang_jasa.xls";
        $judul = "dt_barang_jasa";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Kategori");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama");
	xlsWriteLabel($tablehead, $kolomhead++, "Satuan");
	xlsWriteLabel($tablehead, $kolomhead++, "Spesifikasi");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Update Date");

	foreach ($this->Dt_barang_jasa_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_kategori);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
	    xlsWriteLabel($tablebody, $kolombody++, $data->satuan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->spesifikasi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->update_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Dt_barang_jasa.php */
/* Location: ./application/controllers/Dt_barang_jasa.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-25 11:43:05 */
/* http://harviacode.com */