 <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Jenis Barang</h3>

                <div class="card-tools">
                     <form action="<?php echo site_url('dt_penyedia/index'); ?>" class="form-inline" method="get">
                    <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="q" value="<?php echo $q; ?>" class="form-control float-right" placeholder="Search">
                    <div class="input-group-append">
                         <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('dt_penyedia'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>                  
                </div>
              </div>
              <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                        <th>No</th>
                        <th>Npwp</th>
                        <th>Alamat</th>
                        <th>Nama Penyedia</th>
                        <th>Stts. Pajak</th>
                        <th>Kecamatan</th>
                        <th>Telepon</th>
                        <th>Tanggal Dibuat</th>
                        <th>Action</th>
                    </tr>
                  </thead>
                  <tbody><?php
            foreach ($dt_penyedia_data as $dt_penyedia)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $dt_penyedia->npwp ?></td>
			<td><?php echo $dt_penyedia->alamat ?></td>
			<td><?php echo $dt_penyedia->nama_penyedia ?></td>
			<td><?php echo $dt_penyedia->status_pjk ?></td>
                        <td><?php echo ucfirst($dt_penyedia->nama_kecamatan) ?></td>
			<td><?php echo $dt_penyedia->telepon ?></td>
			<td><?php echo  fdateindo($dt_penyedia->created_date) ?></td>
			<td style="text-align:center" width="200px">
				<?php
				echo anchor(site_url('dt_penyedia/update/'.$dt_penyedia->id_penyedia),'Update'); 
				echo ' | '; 
				echo anchor(site_url('dt_penyedia/delete/'.$dt_penyedia->id_penyedia),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
                  </tbody>
                </table>
                  
              </div>
              <?php echo $pagination ?>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->