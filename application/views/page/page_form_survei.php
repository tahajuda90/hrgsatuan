<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Survei</h3>
                </div>
                            <form action="<?php echo $action; ?>" method="post">
                <div class="card-body">
                    <div class="form-group">
                        <label >BA. Hasil Survei</label>
                        <input type="text" class="form-control" name="nmr_survei" id="nmr_survei" value="<?php echo $nmr_survei; ?>" />
                    </div>
                    <div class="form-group">
                        <label >Tanggal Survei</label>
                        <input type="text" class="form-control" name="tgl_survei" id="tgl_survei" value="<?php echo $tgl_survei; ?>" />
                    </div>
                    <div class="form-group">
                        <label >Keterangan</label>
                        <textarea class="form-control" name="keterangan" id="keterangan" rows="4" value="" ><?php echo $keterangan; ?></textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <input type="hidden" name="id_survei" value="<?php echo $id_survei; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                </div>
            </form>
            </div>

        </div>
    </div>
</div>
