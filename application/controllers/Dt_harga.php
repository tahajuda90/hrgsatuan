<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dt_harga extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Dt_harga_model','Dt_penyedia_model','Dt_barang_jasa_model'));
        $this->load->library('form_validation');
        if (!$this->ion_auth->logged_in()) {
            redirect('/auth', 'refresh');
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'dt_harga/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'dt_harga/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'dt_harga/index.html';
            $config['first_url'] = base_url() . 'dt_harga/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Dt_harga_model->total_rows($q);
        $dt_harga = $this->Dt_harga_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'dt_harga_data' => $dt_harga,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('dt_harga/dt_harga_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Dt_harga_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_objk' => $row->id_objk,
		'id_brg' => $row->id_brg,
		'id_penyedia' => $row->id_penyedia,
		'harga' => $row->harga,
		'created_date' => $row->created_date,
		'update_date' => $row->update_date,
	    );
            $this->load->view('dt_harga/dt_harga_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_harga'));
        }
    }

    public function create($srv) 
    {
        $pnyd = $this->input->get('prvdr',TRUE);
        $brg = $this->Dt_barang_jasa_model->get_all();
        $data = array(
            'page'=>'page_form_harga',
            'barang'=>$brg,
            'button' => 'Create',
            'action' => site_url('dt_harga/create_action/'.$srv),
	    'id_objk' => set_value('id_objk'),
	    'id_brg' => set_value('id_brg'),
            'id_survei' => set_value('id_survei',$srv),
	    'id_penyedia' => set_value('id_penyedia',$pnyd),
	    'harga' => set_value('harga')
	);
        $this->load->view('main', $data);
    }
    
    public function create_action($srv) 
    {
        //$this->_rules();

        //if ($this->form_validation->run() == FALSE) {
        //    $this->create();
        //} else {
            $data = array(
		'id_brg' => $this->input->post('id_brg',TRUE),
		'id_penyedia' => $this->input->post('id_penyedia',TRUE),
                'id_survei' => $this->input->post('id_survei',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
		'harga' => $this->input->post('harga',TRUE)
	    );

            $this->Dt_harga_model->insert($data);
            //$this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('entri_survei/coba/'.$srv.'?prvdr='.$data['id_penyedia']));
        //}
    }
    
    public function update($id) 
    {
        $row = $this->Dt_harga_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('dt_harga/update_action'),
		'id_objk' => set_value('id_objk', $row->id_objk),
		'id_brg' => set_value('id_brg', $row->id_brg),
		'id_penyedia' => set_value('id_penyedia', $row->id_penyedia),
		'harga' => set_value('harga', $row->harga)
	    );
            $this->load->view('dt_harga/dt_harga_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_harga'));
        }
    }
    
    public function update_action() 
    {
//        $this->_rules();
//
//        if ($this->form_validation->run() == FALSE) {
//            $this->update($this->input->post('id_objk', TRUE));
//        } else {
            $data = array(
//		'id_brg' => $this->input->post('id_brg',TRUE),
//		'id_penyedia' => $this->input->post('id_penyedia',TRUE),
//                'id_survei' => $this->input->post('id_survei',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
		'harga' => $this->input->post('harga',TRUE)
	    );

            $this->Dt_harga_model->update($this->input->post('id_objk', TRUE), $data);
//            $this->session->set_flashdata('message', 'Update Record Success');
//            redirect(site_url('dt_harga'));
//        }
    }
    
    public function delete($id) 
    {
        $row = $this->Dt_harga_model->get_by_id($id);

        if ($row) {
            $this->Dt_harga_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('dt_harga'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_harga'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_brg', 'id brg', 'trim|required');
	$this->form_validation->set_rules('id_penyedia', 'id penyedia', 'trim|required');
	$this->form_validation->set_rules('harga', 'harga', 'trim|required');
	$this->form_validation->set_rules('created_date', 'created date', 'trim|required');
	$this->form_validation->set_rules('update_date', 'update date', 'trim|required');

	$this->form_validation->set_rules('id_objk', 'id_objk', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "dt_harga.xls";
        $judul = "dt_harga";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Brg");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Penyedia");
	xlsWriteLabel($tablehead, $kolomhead++, "Harga");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Update Date");

	foreach ($this->Dt_harga_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_brg);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_penyedia);
	    xlsWriteNumber($tablebody, $kolombody++, $data->harga);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->update_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Dt_harga.php */
/* Location: ./application/controllers/Dt_harga.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-23 09:03:48 */
/* http://harviacode.com */