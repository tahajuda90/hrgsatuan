<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tampilfront extends CI_Controller {
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Dt_penyedia_model','Dt_barang_jasa_model','tampil_harga'));
        $this->load->library('form_validation');
    }
    
    public function index(){
        $this->_rules();
        $post = $this->input->post();
        if ($this->form_validation->run() == FALSE) {
            $data = array(
            'kategori' => $this->Dt_barang_jasa_model->get_kategori(),
            'last_survei' => $this->tampil_harga->getLastSurvei(),
            'page'=>'tabel_harga',
            'barang'=>$this->tampil_harga->getHargaBarangAKhir()
        );
        }else{
            $data = array(
            'kategori' => $this->Dt_barang_jasa_model->get_kategori(),
            'last_survei' => $this->tampil_harga->getLastSurvei($post['tgl_survei']),
            'page'=>'tabel_harga',
            'barang'=>$this->tampil_harga->getHargaBarangAKhir($post['tgl_survei'],$post['id_kategori'])
        );
        }
        
        $this->load->view('main_frontend', $data);
    }
    
    public function info_penyedia($idpnyd){
        $data = $this->Dt_penyedia_model->get_by_id($idpnyd);
        echo ' <h1>
                  '.$data->nama_penyedia.'
                </h1> 

                <hr>
                
                <strong> Npwp</strong>
                
                <p class="text-muted">'.$data->npwp.'</p>
                
                <span class="tag tag-info">'.$data->status_pjk.'</span>               
                
                <hr>
                <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

                <p class="text-muted">'.$data->alamat.'</p>
                <p class="text-muted">'.$data->telepon.'</p>';
    }
    
    public function _rules() 
    {
        $this->form_validation->set_rules('id_kategori', 'id_kategori', 'trim');
        $this->form_validation->set_rules('tgl_survei', 'tgl_survei', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}
