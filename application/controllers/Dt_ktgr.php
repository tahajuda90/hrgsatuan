<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dt_ktgr extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Dt_ktgr_model');
        $this->load->library('form_validation');
        if (!$this->ion_auth->logged_in()) {
            redirect('/auth', 'refresh');
        }
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'dt_ktgr?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'dt_ktgr?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'dt_ktgr';
            $config['first_url'] = base_url() . 'dt_ktgr';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Dt_ktgr_model->total_rows($q);
        $dt_ktgr = $this->Dt_ktgr_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'page' => 'page_kategori',
            'dt_ktgr_data' => $dt_ktgr,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('main', $data);
    }

    public function read($id) 
    {
        $row = $this->Dt_ktgr_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_kategori' => $row->id_kategori,
		'kategori' => $row->kategori,
		'keterangan' => $row->keterangan,
		'created_date' => $row->created_date,
		'update_date' => $row->update_date,
	    );
            $this->load->view('dt_ktgr/dt_ktgr_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_ktgr'));
        }
    }

    public function create() 
    {
        $data = array(
            'page' => 'page_form_kategori',
            'button' => 'Create',
            'action' => site_url('dt_ktgr/create_action'),
	    'id_kategori' => set_value('id_kategori'),
	    'kategori' => set_value('kategori'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->load->view('main', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'kategori' => $this->input->post('kategori',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
	    );

            $this->Dt_ktgr_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('dt_ktgr'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Dt_ktgr_model->get_by_id($id);

        if ($row) {
            $data = array(
                'page' => 'page_form_kategori',
                'button' => 'Update',
                'action' => site_url('dt_ktgr/update_action'),
		'id_kategori' => set_value('id_kategori', $row->id_kategori),
		'kategori' => set_value('kategori', $row->kategori),
		'keterangan' => set_value('keterangan', $row->keterangan),
		'created_date' => set_value('created_date', $row->created_date),
		'update_date' => set_value('update_date', $row->update_date),
	    );
            $this->load->view('main', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_ktgr'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_kategori', TRUE));
        } else {
            $data = array(
		'kategori' => $this->input->post('kategori',TRUE),
		'keterangan' => $this->input->post('keterangan',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
	    );

            $this->Dt_ktgr_model->update($this->input->post('id_kategori', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('dt_ktgr'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Dt_ktgr_model->get_by_id($id);

        if ($row) {
            $this->Dt_ktgr_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('dt_ktgr'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('dt_ktgr'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('kategori', 'kategori', 'trim|required');
	$this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
	//$this->form_validation->set_rules('created_date', 'created date', 'trim|required');
	//$this->form_validation->set_rules('update_date', 'update date', 'trim|required');

	$this->form_validation->set_rules('id_kategori', 'id_kategori', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "dt_ktgr.xls";
        $judul = "dt_ktgr";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Kategori");
	xlsWriteLabel($tablehead, $kolomhead++, "Keterangan");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Update Date");

	foreach ($this->Dt_ktgr_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kategori);
	    xlsWriteLabel($tablebody, $kolombody++, $data->keterangan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->update_date);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Dt_ktgr.php */
/* Location: ./application/controllers/Dt_ktgr.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-03-25 07:29:52 */
/* http://harviacode.com */