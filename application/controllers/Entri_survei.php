 <?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Entri_survei
 *
 * @author Tahajuda
 */
class Entri_survei extends CI_Controller {
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Entri_survei_model'));
        $this->load->library('form_validation');
        if (!$this->ion_auth->logged_in()) {
            redirect('Auth', 'refresh');
        }
    }
    
    public function index($srv){
        $q = $this->input->get('q',TRUE);
        $data['srv'] = $srv;
        $data['pnyd'] = $this->input->get('prvdr',TRUE);
        $this->Entri_survei_model->set_prvdr($data['pnyd']);
        $this->Entri_survei_model->set_survei($data['srv']);
        //print_r($this->Entri_survei_model->survei_record());
        $data['record'] = $this->Entri_survei_model->survei_record($q);
        $data['page'] = 'page_isi_survei';
        $this->load->view('main', $data);
//        print_r();
    }
    
    public function coba($srv){
        $q = $this->input->get('q',TRUE);
        $data['srv'] = $srv;
        $data['pnyd'] = $this->input->get('prvdr',TRUE);
        $this->Entri_survei_model->set_prvdr($data['pnyd']);
        $this->Entri_survei_model->set_survei($data['srv']);
        //print_r($this->Entri_survei_model->survei_record());
        $data['record'] = $this->Entri_survei_model->survei_record($q);
        $data['page'] = 'page_isi_survei';
        $this->load->view('main', $data);
    }
}
