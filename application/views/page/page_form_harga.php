<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Harga Barang</h3>
                </div>
                <form action="<?php echo $action; ?>" method="post">
                <div class="card-body">
                    <input type="hidden" class="form-control" name="id_penyedia" id="id_penyedia" value="<?php echo $id_penyedia; ?>" />
                    <input type="hidden" class="form-control" name="id_survei" id="id_survei" value="<?php echo $id_survei; ?>" />
                    <div class="form-group">
                        <label>Pilih Barang</label>
                        <select class="form-control select2" name="id_brg" id="id_brg" style="width: 100%;">
                            <?php
                            foreach ($barang as $brgg) {
                                ?>
                                <option value="<?= $brgg->id_objek ?>"><?= $brgg->nama ?></option>

                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Masukan Harga</label>
                        <input type="text" class="form-control" name="harga" id="harga" placeholder="Harga" value="<?php echo $harga; ?>" />
                    </div>
                </div>
                
                <div class="card-footer">
                    <input type="hidden" name="id_objk" value="<?php echo $id_objk; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                </div>
                </form>
            </div>
        </div>
    </div>
</div>