 <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Kategori</h3>

                <div class="card-tools">
                    <form action="<?php echo site_url('dt_ktgr/index'); ?>" class="form-inline" method="get">
                    <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="q" value="<?php echo $q; ?>" class="form-control float-right" placeholder="Search">
                    <div class="input-group-append">
                        <?php 
                                if ($q <> '')
                                {
                                    ?>
                                    <a href="<?php echo site_url('dt_ktgr'); ?>" class="btn btn-default">Reset</a>
                                    <?php
                                }
                            ?>
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                    </form>                  
                </div>
              </div>
              <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th>Keterangan</th>
                        <th>Tanggal Dibuat</th>
                        <th>Action</th>
                    </tr>
                  </thead>
                  <tbody><?php
            foreach ($dt_ktgr_data as $dt_ktgr)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $dt_ktgr->kategori ?></td>
			<td><?php echo $dt_ktgr->keterangan ?></td>
                        <td><?php echo fdateindo($dt_ktgr->created_date) ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('dt_ktgr/update/'.$dt_ktgr->id_kategori),'Update'); 
				echo ' | '; 
				echo anchor(site_url('dt_ktgr/delete/'.$dt_ktgr->id_kategori),'Delete','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr>
                <?php
            }
            ?>
                  </tbody>
                </table>
                  
              </div>
              <?php echo $pagination ?>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->