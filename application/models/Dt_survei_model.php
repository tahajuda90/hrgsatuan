<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Dt_survei_model extends CI_Model {
    //put your code here
    
    public $table = 'dt_survei';
    public $id = 'id_survei';
    public $order = 'DESC';
    
    public function __construct() {
        parent::__construct();
    }
    
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_survei', $q);
        $this->db->or_like('nmr_survei',$q);
        $this->db->or_like('tgl_survei',$q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_survei', $q);
        $this->db->or_like('nmr_survei',$q);
        $this->db->or_like('tgl_survei',$q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}
