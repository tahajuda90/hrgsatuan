<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Penyedia</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?php echo $action; ?>" method="post">
                <div class="card-body">
                    <div class="form-group">
                        <label >NPWP</label>
                        <input type="text" class="form-control" name="npwp" id="npwp" placeholder="Npwp" value="<?php echo $npwp; ?>" />
                        <select class="form-control select2" name="status_pjk" id="status_pjk" style="width: 100%;">
                            <option></option>
                            <option <?=($status_pjk=="PKP") ? "selected" : ""?> value="PKP">PKP</option>
                            <option <?=($status_pjk=="PTKP") ? "selected" : ""?> value="PTKP">PTKP</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label >Nama Penyedia</label>
                        <input type="text" class="form-control" name="nama_penyedia" id="nama_penyedia" placeholder="Nama Penyedia" value="<?php echo $nama_penyedia; ?>" />
                    </div>
                    <div class="form-group">
                        <label >Telepon</label>
                        <input type="text" class="form-control" name="telepon" id="telepon" placeholder="Telepon" value="<?php echo $telepon; ?>" />
                    </div>
                    <div class="form-group">
                        <label >Alamat</label>
                        <select class="form-control select2" name="id_kecamatan" id="id_kecamatan" style="width: 100%;">
                            <option></option>
                            <?php
                            foreach($kecamatan as $kcmt){    
                            ?>
                            <option <?=($kcmt->id_kecamatan==$id_kecamatan) ? "selected" : ""?> value="<?=$kcmt->id_kecamatan?>">Kecamatan <?= ucfirst($kcmt->nama_kecamatan)?></option>    
                            <?php
                            }
                            ?>
                        </select>
                        <textarea class="form-control" rows="4" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <input type="hidden" name="id_penyedia" value="<?php echo $id_penyedia; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->

