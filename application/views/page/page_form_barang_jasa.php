<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Kategori</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?php echo $action; ?>" method="post">
                <div class="card-body">
                    <div class="form-group">
                        <label>Pilih Kategori</label>
                        <select class="form-control select2" name="id_kategori" id="id_kategori" style="width: 100%;">
                            <?php
                            foreach($kategori as $ktgr){
                                ?>
                            <option <?=($ktgr->id_kategori==$id_kategori) ? "selected" : ""?> value="<?=$ktgr->id_kategori?>"><?=$ktgr->kategori?></option>
                                    
                                    <?php
                            }
                            ?>
                        </select>
                    </div>
                  <div class="form-group">
                    <label >Nama Barang</label>
                    <input type="text" class="form-control" name="nama" id="nama" value="<?php echo $nama; ?>" />
                  </div>
                  <div class="form-group">
                    <label >Satuan Barang</label>
                    <input type="text" class="form-control" name="satuan" id="satuan" value="<?php echo $satuan; ?>" />
                  </div>
                  <div class="form-group">
                    <label >Spesifikasi</label>
                    <textarea class="form-control" rows="4" name="spesifikasi" id="spesifikasi" ><?php echo $spesifikasi; ?></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <input type="hidden" name="id_objek" value="<?php echo $id_objek; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
