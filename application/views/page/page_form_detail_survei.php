<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Tambah Daftar Penyedia</h3>
                </div>
                <form action="<?php echo $action; ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label >Pilih Penyedia</label>
                            <select class="form-control select2" name="id_penyedia" id="id_kategori" style="width: 100%;">
                                <?php
                            foreach($pilihan as $plhn){
                                ?>
                                <option value="<?=$plhn->id_penyedia?>"><?=$plhn->nama_penyedia?></option>
                                <?php
                            }
                            ?>
                            </select>
                        </div>
                        <input class="form-control" name="id_survei" value="<?=$idsurvei?>" type="hidden" value=""/>
                        <input class="form-control" name="id_detail_penyedia" type="hidden" value=""/>
                        <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>