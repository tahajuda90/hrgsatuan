<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dt_survei extends CI_Controller {
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model(array('Dt_survei_model','Dt_survei_penyedia_model','Dt_penyedia_model'));
        $this->load->library('form_validation');
        if(!$this->ion_auth->logged_in()){
            redirect('Auth','refresh');
        }
    }
    
    public function index(){
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'dt_survei?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'dt_survei?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'dt_survei';
            $config['first_url'] = base_url() . 'dt_survei';
        }
        
        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Dt_survei_model->total_rows($q);
        
        $dt_survei = $this->Dt_survei_model->get_limit_data($config['per_page'], $start, $q);
        
        $this->load->library('pagination');
        $this->pagination->initialize($config);
        
        $data = array(
            'page' => 'page_survei',
            'dt_survei_data'=>$dt_survei,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
        );
        $this->load->view('main', $data);
    }
    
    public function create() {
        $data = array(
            'page' => 'page_form_survei',
            'button' => 'Create',
            'action' => site_url('dt_survei/create_action'),
            'id_survei'=> set_value('id_survei'),
	    'nmr_survei' => set_value('nmr_survei'),
	    'tgl_survei' => set_value('tgl_survei'),
	    'keterangan' => set_value('keterangan'),
	);
        $this->load->view('main', $data);
    }
    
    public function create_action(){
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nmr_survei'=>$this->input->post('nmr_survei',TRUE),
                'tgl_survei'=> fdatetimetodb($this->input->post('tgl_survei',TRUE)),
                'keterangan'=>$this->input->post('keterangan',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
            );
            $this->Dt_survei_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('dt_survei'));
        }
        
    }
    
    public function update($id){
        
    }
    
    public function update_action(){
        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nmr_survei'=>$this->input->post('nmr_survei',TRUE),
                'tgl_survei'=>$this->input->post('tgl_survei',TRUE),
                'keterangan'=>$this->input->post('keterangan',TRUE),
                'audituser' =>  $this->ion_auth->user()->row()->username,
            );
            $this->dt_survei_model->update($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('dt_survei'));
        }
    }
    
    public function delete($id){
        
    }
    
    public function detail($id){
        $dt_pnyd = $this->Dt_survei_penyedia_model->get_by_id($id);
        $data = array(
            'page' => 'page_detail_survei',
            'dt_penyedia_data'=>$dt_pnyd,
            'idsurvei'=>$id
        );
        $this->load->view('main', $data);
    }
    
    public function create_detail($idsurvei){
        $dt_pnyd = $this->Dt_penyedia_model->get_all();
        $data = array(
            'idsurvei'=>$idsurvei,
            'action'=> base_url('dt_survei/create_action_detail'),
            'page' => 'page_form_detail_survei',
            'pilihan'=>$dt_pnyd,
            'button'=>'Create'
        );
        $this->load->view('main',$data);
    }
    
    public function create_action_detail(){
         $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->create_detail();
        } else {
            $data = array(
                'id_survei'=>$this->input->post('id_survei',TRUE),
                'id_penyedia'=>$this->input->post('id_penyedia',TRUE)
            );
            $this->Dt_survei_penyedia_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('dt_survei/detail/'.$data['id_survei']));
//            print_r($this->input->post());
        }        
    }


    public function _rules() 
    {
        $this->form_validation->set_rules('id_survei', 'id_survei', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
}
