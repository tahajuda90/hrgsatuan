/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100128
 Source Host           : localhost:3306
 Source Schema         : hrg_satuan

 Target Server Type    : MySQL
 Target Server Version : 100128
 File Encoding         : 65001

 Date: 23/09/2020 11:50:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dt_barang_jasa
-- ----------------------------
DROP TABLE IF EXISTS `dt_barang_jasa`;
CREATE TABLE `dt_barang_jasa`  (
  `id_objek` int(64) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(64) NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `satuan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `spesifikasi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `audituser` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_objek`) USING BTREE,
  INDEX `kat_barjas`(`id_kategori`) USING BTREE,
  CONSTRAINT `kat_barjas` FOREIGN KEY (`id_kategori`) REFERENCES `dt_ktgr` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dt_detail_survei_penyedia
-- ----------------------------
DROP TABLE IF EXISTS `dt_detail_survei_penyedia`;
CREATE TABLE `dt_detail_survei_penyedia`  (
  `id_detail_penyedia` int(64) NOT NULL AUTO_INCREMENT,
  `id_survei` int(64) NULL DEFAULT NULL,
  `id_penyedia` int(64) NULL DEFAULT NULL,
  PRIMARY KEY (`id_detail_penyedia`) USING BTREE,
  INDEX `dtail_srv`(`id_survei`) USING BTREE,
  INDEX `dtail_pnyd`(`id_penyedia`) USING BTREE,
  CONSTRAINT `dtail_pnyd` FOREIGN KEY (`id_penyedia`) REFERENCES `dt_penyedia` (`id_penyedia`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dtail_srv` FOREIGN KEY (`id_survei`) REFERENCES `dt_survei` (`id_survei`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dt_harga
-- ----------------------------
DROP TABLE IF EXISTS `dt_harga`;
CREATE TABLE `dt_harga`  (
  `id_objk` int(64) NOT NULL AUTO_INCREMENT,
  `id_brg` int(64) NULL DEFAULT NULL,
  `id_penyedia` int(64) NULL DEFAULT NULL,
  `id_survei` int(64) NULL DEFAULT NULL,
  `harga` int(255) NULL DEFAULT NULL,
  `audituser` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_objk`) USING BTREE,
  INDEX `hr_barg`(`id_brg`) USING BTREE,
  INDEX `hr_survei`(`id_survei`) USING BTREE,
  INDEX `hr_penyedia`(`id_penyedia`) USING BTREE,
  CONSTRAINT `hr_barg` FOREIGN KEY (`id_brg`) REFERENCES `dt_barang_jasa` (`id_objek`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hr_penyedia` FOREIGN KEY (`id_penyedia`) REFERENCES `dt_penyedia` (`id_penyedia`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hr_survei` FOREIGN KEY (`id_survei`) REFERENCES `dt_survei` (`id_survei`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dt_ktgr
-- ----------------------------
DROP TABLE IF EXISTS `dt_ktgr`;
CREATE TABLE `dt_ktgr`  (
  `id_kategori` int(64) NOT NULL AUTO_INCREMENT,
  `kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `audituser` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dt_penyedia
-- ----------------------------
DROP TABLE IF EXISTS `dt_penyedia`;
CREATE TABLE `dt_penyedia`  (
  `id_penyedia` int(64) NOT NULL AUTO_INCREMENT,
  `id_kecamatan` int(24) NULL DEFAULT NULL,
  `audituser` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `npwp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `nama_penyedia` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_pjk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telepon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_penyedia`) USING BTREE,
  INDEX `kcmtan`(`id_kecamatan`) USING BTREE,
  CONSTRAINT `kcmtan` FOREIGN KEY (`id_kecamatan`) REFERENCES `kecamatan` (`id_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for dt_survei
-- ----------------------------
DROP TABLE IF EXISTS `dt_survei`;
CREATE TABLE `dt_survei`  (
  `id_survei` int(64) NOT NULL AUTO_INCREMENT,
  `nmr_survei` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_survei` datetime(6) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `audituser` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_survei`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'members', 'General User');

-- ----------------------------
-- Table structure for kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `kecamatan`;
CREATE TABLE `kecamatan`  (
  `id_kecamatan` int(24) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(125) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kecamatan
-- ----------------------------
INSERT INTO `kecamatan` VALUES (1, 'pesantren');
INSERT INTO `kecamatan` VALUES (2, 'kota');
INSERT INTO `kecamatan` VALUES (3, 'mojoroto');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remember_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_email`(`email`) USING BTREE,
  UNIQUE INDEX `uc_activation_selector`(`activation_selector`) USING BTREE,
  UNIQUE INDEX `uc_forgotten_password_selector`(`forgotten_password_selector`) USING BTREE,
  UNIQUE INDEX `uc_remember_selector`(`remember_selector`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'administrator', '$2y$12$LLSXeYCFWaNqu3QxypQhZ.lt3IIN0VhdMNb64YbHlUa3m5T6vaRGS', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1600834562, 1, 'Admin', 'istrator', 'adminbpbj', '0');
INSERT INTO `users` VALUES (2, '::1', 'tajuda', '$2y$10$Cf9HsTB.DpNMsIOSufpvhe/vNmWM3nKy.mE8plQ.NzJ78DbWGTRLu', 'tajuda@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1599198206, 1600665019, 1, 'tahajud', 'mandari', 'bpbj', '08328741860');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_users_groups`(`user_id`, `group_id`) USING BTREE,
  INDEX `fk_users_groups_users1_idx`(`user_id`) USING BTREE,
  INDEX `fk_users_groups_groups1_idx`(`group_id`) USING BTREE,
  CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (4, 1, 1);
INSERT INTO `users_groups` VALUES (5, 1, 2);
INSERT INTO `users_groups` VALUES (3, 2, 2);

-- ----------------------------
-- Function structure for getharga
-- ----------------------------
DROP FUNCTION IF EXISTS `getharga`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `getharga`(`f_idbrg` INT, `f_idkcmtn` INT, `tglakhir` DATETIME) RETURNS int(11)
    DETERMINISTIC
BEGIN
 DECLARE hsl INT;
  SELECT harga INTO hsl FROM dt_survei m
	inner join dt_detail_survei_penyedia d on m.id_survei=d.id_survei
	inner join dt_penyedia p on p.id_penyedia=d.id_penyedia
	inner join dt_harga h on h.id_penyedia=d.id_penyedia
	WHERE h.id_brg=f_idbrg AND p.id_kecamatan= f_idkcmtn AND m.tgl_survei<=tglakhir
	order by m.tgl_survei desc 
	limit 1;
  RETURN hsl;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
