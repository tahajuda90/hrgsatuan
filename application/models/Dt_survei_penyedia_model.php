<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dt_survei_penyedia_model extends CI_Model{
    //put your code here
    public $table = 'dt_detail_survei_penyedia';
    public $table2 = 'dt_penyedia';
    public $table3 = 'dt_survei';
    public $id = 'id_detail_penyedia';
    public $id2 = 'id_penyedia';
    public $id3 = 'id_survei';
    public $order = 'DESC';
    
    
    public function __construct() {
        parent::__construct();
    }    
    
    
    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->join($this->table2, $this->table.'.'.$this->id2.'='.$this->table2.'.'.$this->id2);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->table.'.'.$this->id3, $id);
        $this->db->join($this->table2, $this->table.'.'.$this->id2.'='.$this->table2.'.'.$this->id2);
        $this->db->join($this->table3, $this->table.'.'.$this->id3.'='.$this->table3.'.'.$this->id3);
        return $this->db->get($this->table)->result();
    }
    
    // get total rows
    function total_rows($q = NULL) {
//        $this->db->like('id_survei', $q);
//        $this->db->or_like('nmr_survei',$q);
//        $this->db->or_like('tgl_survei',$q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
//        $this->db->like('id_survei', $q);
//        $this->db->or_like('nmr_survei',$q);
//        $this->db->or_like('tgl_survei',$q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }
    
    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }
}
