<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tampil_harga extends CI_Model {
    //put your code here
    public function __construct() {
        parent::__construct();
    }
    
    public function get_penyedia(){
        $this->db->select('*');
        $this->db->from('dt_penyedia');
        return $this->db->get()->result();
    }
    
    public function get_barjas(){
        $this->db->select('*');
        $this->db->from('dt_barang_jasa');
        return $this->db->get()->result();
    }
    
    private function get_harga($idpnyd,$idbrg){
        $this->db->select('dt_harga.harga');
        $this->db->from('dt_harga');
        $this->db->where('dt_harga.id_penyedia',$idpnyd);
        $this->db->where('dt_harga.id_brg',$idbrg);
        $this->db->order_by('dt_harga.created_date','DESC');
        $this->db->limit(1);
        $result = $this->db->get();
        if($result->num_rows()>0){
            return $result->result()[0]->harga;
        }else{
            return "0";
        }
            
                
    }
    
    public function content(){
        $penyedia = $this->get_penyedia();
        $barang = $this->get_barjas();
        $oke = array();
        foreach ($penyedia as $pny){
            $tbl = '<tr><td>'.$pny->nama_penyedia.'</td>';
            foreach ($barang as $brg){
                $tbl .= '<td>'.$this->tampil_harga->get_harga($pny->id_penyedia,$brg->id_objek).'</td>';
            }
            $tbl .= '</tr>';
            array_push($oke, $tbl);
            $tbl = NULL;
        };
        return $oke;
    }
    
    public function header(){
        $barang = $this->get_barjas();
        $oke = '<tr><th>Penyedia</th>';
        foreach($barang as $brg){
            $oke .='<th>'. ucfirst($brg->nama).'</th>';
        }
        $oke .='</tr>';
        return $oke;
    }
    
    function getHargaBarangAKhir($tglakhir=null,$ktgr=null){
        if($tglakhir==null){
            $tglakhir = fdatetodb("now");
        }
        
        $skrip = "SELECT
	b.id_objek,
	c.kategori,
	b.nama,
	b.satuan,
	b.spesifikasi,
(select h.harga from dt_survei m
	inner join dt_detail_survei_penyedia d on m.id_survei=d.id_survei
	inner join dt_penyedia p on p.id_penyedia=d.id_penyedia
	inner join dt_harga h on h.id_penyedia=d.id_penyedia
	where h.id_brg=b.id_objek and p.id_kecamatan=1 and m.tgl_survei<='$tglakhir'
	order by m.tgl_survei desc, h.update_date desc
	LIMIT 1
	) as HargaPesantren,
	(select h.id_penyedia from dt_survei m
	inner join dt_detail_survei_penyedia d on m.id_survei=d.id_survei
	inner join dt_penyedia p on p.id_penyedia=d.id_penyedia
	inner join dt_harga h on h.id_penyedia=d.id_penyedia
	where h.id_brg=b.id_objek and p.id_kecamatan=1 and m.tgl_survei<='$tglakhir'
	order by m.tgl_survei desc, h.update_date desc
	LIMIT 1
	) as PnydPesantren,
	(select h.harga from dt_survei m
	inner join dt_detail_survei_penyedia d on m.id_survei=d.id_survei
	inner join dt_penyedia p on p.id_penyedia=d.id_penyedia
	inner join dt_harga h on h.id_penyedia=d.id_penyedia
	where h.id_brg=b.id_objek and p.id_kecamatan=2 and m.tgl_survei<='$tglakhir'
	order by m.tgl_survei desc, h.update_date desc
	LIMIT 1
	) as HargaKota,
	(select h.id_penyedia from dt_survei m
	inner join dt_detail_survei_penyedia d on m.id_survei=d.id_survei
	inner join dt_penyedia p on p.id_penyedia=d.id_penyedia
	inner join dt_harga h on h.id_penyedia=d.id_penyedia
	where h.id_brg=b.id_objek and p.id_kecamatan=2 and m.tgl_survei<='$tglakhir'
	order by m.tgl_survei desc, h.update_date desc
	LIMIT 1
	) as PnydKota,
	(select h.harga from dt_survei m
	inner join dt_detail_survei_penyedia d on m.id_survei=d.id_survei
	inner join dt_penyedia p on p.id_penyedia=d.id_penyedia
	inner join dt_harga h on h.id_penyedia=d.id_penyedia
	where h.id_brg=b.id_objek and p.id_kecamatan=3 and m.tgl_survei<='$tglakhir'
	order by m.tgl_survei desc, h.update_date desc
	LIMIT 1
	) as HargaMojoroto,
	(select h.id_penyedia from dt_survei m
	inner join dt_detail_survei_penyedia d on m.id_survei=d.id_survei
	inner join dt_penyedia p on p.id_penyedia=d.id_penyedia
	inner join dt_harga h on h.id_penyedia=d.id_penyedia
	where h.id_brg=b.id_objek and p.id_kecamatan=3 and m.tgl_survei<='$tglakhir'
	order by m.tgl_survei desc, h.update_date desc
	LIMIT 1
	) as PnydMojoroto
	
FROM
	dt_barang_jasa b
INNER JOIN dt_ktgr c ON b.id_kategori=c.id_kategori";
        if($ktgr!=null){
         $query = $this->db->query($skrip." WHERE c.id_kategori = $ktgr");   
        }else{
        $query = $this->db->query($skrip);    
        }        
        return $query->result();
    }
    
    function getLastSurvei($tglakhir=null){
        if($tglakhir==null){
            $tglakhir = fdatetodb("now");
        }
        $this->db->where('dt_survei.tgl_survei <=',$tglakhir);
        return $this->db->get('dt_survei')->row();
    }
}
