<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Kategori</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?php echo $action; ?>" method="post">
                <div class="card-body">
                  <div class="form-group">
                    <label >Nama Kategori</label>
                    <input type="text" class="form-control" name="kategori" id="kategori" value="<?php echo $kategori; ?>" />
                  </div>
                  <div class="form-group">
                    <label >Keterangan</label>
                    <textarea class="form-control" name="keterangan" id="keterangan" rows="4" value="" ><?php echo $keterangan; ?></textarea>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <input type="hidden" name="id_kategori" value="<?php echo $id_kategori; ?>" /> 
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
