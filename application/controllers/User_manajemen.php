<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of User_manajemen
 *
 * @author Tahajuda
 */

class User_manajemen extends CI_Controller {
    //put your code here
    
    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        
        if (!$this->ion_auth->logged_in()&&!$this->ion_auth->in_group('admin')) {
            redirect('/auth', 'refresh');
        }
    }
    
    public function index()
    {
        $data = array(
            'user'=>$this->user_model->get_all_users(),
            'page'=>'page_usermanajemen'
        );
        $this->load->view('main', $data);
    }
    

    
}
